const {Router}  = require('express');
const bicicletaController = require('../controllers/bicicleta')
const router = Router();


router.get('/' ,bicicletaController.list);

router.get('/create',bicicletaController.create);

router.post('/create',bicicletaController.add);

router.get('/delete/:id',bicicletaController.delete);

router.get('/update/:id',bicicletaController.list_one);

router.post('/update/:id',bicicletaController.update);




module.exports = router;



var express = require('express');
var router = express.Router();
const controller = require('../controllers/bicicleta')

/* GET home page. */
router.get('/', controller.list);

module.exports = router;

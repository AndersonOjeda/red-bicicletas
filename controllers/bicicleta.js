const bicicletamodel = require ('../models/bicicleta');


const bicicleta = {}


bicicleta.list =(req,res,next) => {
    res.render('index', { bicis: bicicletamodel.allBicis });
}

bicicleta.vie_list= () => {
   return (bicicletamodel.allBicis)
}

bicicleta.create = (req,res)  => {
    res.render('bicicleta/create',{status: 'received'})
}

bicicleta.add = (req,res) => {
    const {color,modelo,latitud,longitud} = req.body

    

    const nBicicleta = {
        color,
        modelo,
        ubicacion : [latitud , longitud]  
    }
    
    bicicletamodel.add(nBicicleta);

    res.redirect('/');
}


bicicleta.delete = (req,res) => {
    const {id} = req.params
    bicicletamodel.delete(id) ;
  res.redirect('/bicicleta')
}


bicicleta.list_one = (req,res) =>{

    const {id} = req.params

    const bici = bicicletamodel.allBicis.filter(item => item.id == id );

   
    res.render('bicicleta/update',{
        bici: bici
    });
}

bicicleta.update = (req,res) => {
  const {color,modelo,latitud,longitud} = req.body
    const id = parseInt(req.params.id);
  const nBici = {
      id,
      color,
      modelo,
      ubicacion: [latitud,longitud]
  }

     bicicletamodel.update( id ,nBici);
     res.redirect('/');
}


module.exports = bicicleta;


